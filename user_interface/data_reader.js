
function _loadJSON(path, success, error)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
                    success(JSON.parse(xhr.responseText));
            } else {
                if (error)
                    error(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
}

function _parseList(data, tgt)
{
    var btn = data.buttons;

    console.log(data.buttons);

    var ul = document.createElement("UL");
    for (var i = 0; i < btn.length; ++i)
    {
        var li = document.createElement("LI");
        var anc = document.createElement('A');
            anc.setAttribute("HREF", btn[i].link);
        
        var img = document.createElement("IMG");
            img.src = btn[i].image;
        var h3 = document.createElement("H3");
            h3.innerHTML = btn[i].caption_en;
        var p = document.createElement("P");
            p.innerHTML = btn[i].description_en;

        anc.appendChild(img);
        anc.appendChild(h3);
        anc.appendChild(p);

        li.appendChild(anc);

        //li.setAttribute('data-name', bnt[i].name);

        ul.appendChild(li);
    }
    tgt.appendChild(ul);
}


function _loadListItems()
{
    var tgt = document.getElementById("main_list");

    _loadJSON("data.json",
            function(data) { _parseList(data, tgt);}
         ,  function(xhr) { tgt.innerHTML = xhr; }
    );

}

function _loadLanguages()
{

    var tgt = document.getElementById("lang_list");

    var ul = document.createElement("UL");

    var lang = ["EN", "GE", "FR", "IT"];

    for (var i = 0; i < lang.length; ++i)
    {
        var li = document.createElement("LI");

        var anc = document.createElement('A');
            anc.setAttribute("HREF", "#");
            anc.innerHTML = lang[i];

        li.appendChild(anc);
        ul.appendChild(li);
    }

    tgt.appendChild(ul);
}

function loadIndex()
{
    _loadListItems();
    _loadLanguages();
}
