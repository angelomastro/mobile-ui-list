
# === main functions to evaluate ===

def addEntry(dicOut, dicInp):
    """ this function adds a new entry (dictionary) into the 
        original dicOut dictionary
    """
    # necessary input check
    if (type(dicOut) is not dict or 
        type(dicInp) is not dict or len(dicInp) == 0 or 
        ('num_items' in dicInp and 'amount' not in dicInp) or 
        ('num_items' not in dicInp and 'amount' in dicInp)):
        print "wrong items" + str(dicInp)
        return

    # # hading the 'inception-case' (dict in a dict in a dict ...) 
    # if ('num_items' not in dicInp or 'amount' not in dicInp):
    #     for k, v in dicInp:
    #         if type(v) is dict:
    #             if k not in dicOut:
    #                 dicOut[k] = {}
    #                 addEntry(dicOut[k], dicInp[k])

    # adding necesary fields in output, if missing 
    if 'num_items' not in dicOut:
        dicOut['num_items'] = 0
    if 'amount' not in dicOut:
        dicOut['amount'] = 0
    if 'count' not in dicOut:
        dicOut['count'] = 0
    
    # caching amount and num_items, filling main values in output
    amt = dicInp['amount']
    numit = dicInp['num_items']
    cnt = 1 if 'count' not in dicInp else dicInp['count']
    
    dicOut['count'] += cnt
    dicOut['num_items'] += numit
    dicOut['amount'] += amt

    # iterating through the main input values and writing them in output
    for k, v in dicInp.iteritems():
        if k not in ('amount', 'count', 'num_items'):
            if k in dicOut: 
                if type(v) is not dict:
                    if v in dicOut[k]:
                        dicOut[k][v]['count'] += cnt 
                        dicOut[k][v]['num_items'] += numit
                        dicOut[k][v]['amount'] += amt
                    else:
                        dicOut[k] = {str(v): {'count': cnt, 
                                    'num_items': numit, 'amount': amt}}
                else:
                    for it in v:
                        if it not in dicOut[k]:
                            dicOut[k][it] = {}
                        addEntry(dicOut[k][it], dicInp[k][it])
            else:
                if type(v) is not dict:
                    dicOut[k] = {str(v): {'count': cnt, 'num_items': numit, 
                                          'amount': amt}}
                else:
                    dicOut[k] = {}
                    for it in v:
                        dicOut[k][it] = {}
                        addEntry(dicOut[k][it], dicInp[k][it])



def sumAllMonths(dicOut, lisMon):
    """ this function adds up the items of the lisMon dictionaries
        into the output dicOut dictionary
    """
    for dicInp in lisMon:
        addEntry(dicOut, dicInp)


# === Utility functions to parse text files in JSON ===
import json
def _byteify(data, ignore_dicts = False):
    # this function transforms unicode to str objects 
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): 
                _byteify(value, ignore_dicts=True)
                for key, value in data.iteritems()
        }
    # if it's anything else, return it in its original form
    return data


def parseJsonToDict(fileName):
    ret = {}
    with open(fileName, 'r') as f:
        js = f.read()
        ret = _byteify(json.loads(js, object_hook=_byteify),
                ignore_dicts=True)
    return ret



if __name__ == "__main__":
    import copy
    dicTot = parseJsonToDict("october.json")
    dicOri = copy.deepcopy(dicTot)

    #1. testing a new entry
    print "     testing new entry ... "
    entry = parseJsonToDict("new_entry.json")
    addEntry(dicTot, entry)

    assert(dicTot['count'] == dicOri['count'] + 1)
    assert(dicTot['gender']['M']['count'] == 
                            dicOri['gender']['M']['count'] + 1)
    assert(dicTot['gender']['F']['count'] == dicOri['gender']['F']['count'] )

    #2. testing a new entry with a new value for a given key
    print "     testing new value for existing key ... "
    del dicTot  
    dicTot = copy.deepcopy(dicOri)
    entry_key = parseJsonToDict("new_entry_key.json")
    addEntry(dicTot, entry_key)

    assert(dicTot['count'] == dicOri['count'] + 1)
    assert(dicTot['currency']['GBP']['count'] == 1)

    #3. testing a new entry with a new key 
    print "     testing new key and value ... "
    del dicTot  
    dicTot = copy.deepcopy(dicOri)
    entry_val = parseJsonToDict("new_entry_val.json")
    addEntry(dicTot, entry_val)

    assert(dicTot['count'] == dicOri['count'] + 1)
    assert(dicTot['weekOfYear']['25']['count'] == 1)

    #4. testing the sum of of a new month (november)
    #   including the three cases tested above in one go
    dicNew = parseJsonToDict("november.json")
    dicSum = {}
    sumAllMonths(dicSum, [dicOri, dicNew])
    print "     testing the sum up of two months ... "
    assert(dicSum['count'] == 25)
    print "     testing the sum up of two months for existing key ... "
    assert(dicSum['currency']['EUR']['count'] == 15)
    print "     testing the sum up of two months for new key ... "
    assert(dicSum['currency']['GBP']['count'] == 3)
    print "     testing the sum up of two months for new value ... "
    assert(dicSum['weekOfYear']['49']['count'] == 5)

    # print dicSum
    print ">> all tests have passed :D"




# ,
#    "inception":{
#       "dream":{
#          "in-a-dream":{
#             "in-a-another-dream":{
#                "count":10,
#                "amount":250,
#                "num_items":8,
#                "status": "crazy"
#             }
#          }
#       }
#    }

